import mock
import pytest
from dict_tools import data


@pytest.fixture
def hub(hub):
    hub.pop.sub.add(dyne_name="idem")
    hub.OPT = mock.MagicMock()
    return hub


@pytest.fixture
def ctx():
    """
    Override pytest-pop's ctx fixture with one that has a mocked session
    """
    yield data.NamespaceDict(
        acct=data.NamespaceDict(
            token=data.NamespaceDict(acces_token="mock_token"),
            csp_url="mock_csp_url",
            org_id="mock_org_id",
        )
    )
